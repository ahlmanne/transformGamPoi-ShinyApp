
# Source code for the Shiny of _Comparison of Transformations for Single-Cell RNA-Seq Data_

This repository contains the code for a Shiny app that displays the benchmark results from our [Comparison of Transformations for Single-Cell RNA-Seq Data](https://www.nature.com/articles/s41592-023-01814-1) paper.

The app is available at https://shiny-portal.embl.de/shinyapps/app/08_single-cell_transformation_benchmark.


